﻿using System;
using System.Diagnostics;
// Тестовые модули
using TestJob;
using TestApp.SomeSpace;
using Class2 = TestApp.AnotherSpace.Class2;

namespace Conteq.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var class1 = new Class1();
            var class2 = new Class2(5);

            Debug.Assert(class1.Number == 10);
            Debug.Assert(class1.Calculate(3) == 3 * 3 * 10);
            Debug.Assert(class2.Calculate(15, 5) == 15 * 5 * 5);

            Debug.Assert(class1.IsFirst == true);
            Debug.Assert(class2.IsFirst == false);

            Debug.Assert(class1.IsSecond == false);
            Debug.Assert(class2.IsSecond == true);

            Debug.Assert(class1 is First);
            Debug.Assert(class2 is Second);

            Debug.Assert(class1 is Parent);
            Debug.Assert(class2 is Parent);

            try
            {
                class1.Calculate(7);
                Debug.Assert(false);
            }
            catch (MyException exc)
            {
                Debug.Assert(exc.Message == "Error Message");
            }

            try
            {
                class1.IsSecond = true;
                Debug.Assert(false);
            }
            catch (ArgumentException)
            {
            }

            Console.WriteLine("done");
        }
    }
}
