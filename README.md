Для избавления от дублирующейся части кода, мы могли бы перенести реализацию методов **IsFirst** и **IsSecond** в класс `Parent` следующим образом:

```java
public override bool IsFirst
{
    get => this is First;
    set
    {
        if (!((this is First) ^ value))
        {
            throw new ArgumentException();
        }
    }
}

public override bool IsSecond
{
    get => this is Second;
    set
    {
        if (!(this is Second && value))
        {
            throw new ArgumentException();
        }
    }
}
```

Но подобная констрункция содержит зависимость родительского компонента от дочернего, что нарушает концепцию принципа подстановки Лисков