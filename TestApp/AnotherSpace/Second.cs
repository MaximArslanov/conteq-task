﻿using System;

namespace TestApp.SomeSpace
{
    public abstract class Second : Parent
    {
        public override bool IsFirst
        {
            get => false;
            set
            {
                if (value)
                {
                    throw new ArgumentException();
                }
            }
        }

        public override bool IsSecond
        {
            get => true;
            set
            {
                if (!value)
                {
                    throw new ArgumentException();
                }
            }
        }
    }
}