﻿namespace TestApp.SomeSpace
{
    public abstract class Parent
    {
        public int Number { get; set; }

        public abstract bool IsFirst { get; set; }
        public abstract bool IsSecond { get; set; }

        public int Calculate(int x, int? y = null)
        {
            if (y.HasValue)
            {
                return x * y.Value * Number;
            }
            else
            {
                return x * x * Number;
            }
        }
    }
}
