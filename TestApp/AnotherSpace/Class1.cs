﻿namespace TestApp.SomeSpace
{
    public class Class1 : First
    {
        public Class1()
        {
            Number = 10;
        }

        public new int Calculate(int x, int? y = null)
        {
            if (x == 7)
            {
                throw new MyException("Error Message");
            }

            return base.Calculate(x, y);
        }
    }
}
