﻿using System;

namespace TestApp.SomeSpace
{
    public abstract class First : Parent
    {
        public override bool IsFirst
        {
            get => true;
            set
            {
                if (!value)
                {
                    throw new ArgumentException();
                }
            }
        }

        public override bool IsSecond
        {
            get => false;
            set
            {
                if (value)
                {
                    throw new ArgumentException();
                }
            }
        }
    }
}
