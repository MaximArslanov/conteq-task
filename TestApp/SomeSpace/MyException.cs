﻿using System;

namespace TestApp.SomeSpace
{
    public class MyException : Exception
    {
        public MyException()
        {
        }

        public MyException(string message) : base(message)
        {
        }
    }
}
